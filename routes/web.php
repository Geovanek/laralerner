<?php
Route::group(['namespace' => 'Site'], function() {
    Route::get('/', 'SiteController@index')->name('home');
    Route::get('/home', 'SiteController@index')->name('home');

    Route::get('/contato', 'SiteController@contato')->name('contact');

    Route::get('/categoria/{id}', 'SiteController@categoria')->name('category');
});

/*
/-----------------------
/ Descrição
/-----------------------
/ É passado o filtro para estar logado
/ É passado o namespace no grupo, para não precisar especificar sempre na roda
/ É definido um name (não lembro pq)
/
*/
$this->group(['middleware' => ['auth'], 'namespace' => 'Admin', 'prefix' => 'admin'], function(){
    $this->any('historic-search', 'BalanceController@searchHistoric')->name('historic.search');
    $this->get('historic', 'BalanceController@historic')->name('admin.historic');

    $this->post('transfer', 'BalanceController@transferStore')->name('transfer.store');
    $this->post('confirm-transfer', 'BalanceController@confirmTransfer')->name('confirm.transfer');
    $this->get('transfer', 'BalanceController@transfer')->name('balance.transfer');

    $this->post('withdraw', 'BalanceController@withdrawStore')->name('withdraw.store');
    $this->get('withdraw', 'BalanceController@withdraw')->name('balance.withdraw');

    $this->post('deposit', 'BalanceController@depositStore')->name('deposit.store');
    $this->get('deposit', 'BalanceController@deposit')->name('balance.deposit');
    $this->get('balance', 'BalanceController@index')->name('admin.balance');

    $this->get('/', 'AdminController@index')->name('admin.home');
});

$this->post('atualizar-perfil', 'Admin\UserController@profileUpdate')->name('profile.update')->middleware('auth');
$this->get('meu-perfil', 'Admin\UserController@profile')->name('profile')->middleware('auth');

$this->group(['namespace' => 'Posts'], function(){
    $this->resource('posts', 'PostController');
    $this->post('comment', 'CommentController@store')->name('comment.store');
});

Auth::routes();

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
