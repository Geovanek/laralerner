<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'      => 'Geovane Krüger',
            'email'     => 'geovanek@gmail.com',
            'password'  => bcrypt('RP#2008#gt'),
        ]);

        User::create([
            'name'      => 'Teste',
            'email'     => 'teste@teste.gk',
            'password'  => bcrypt('RP#2008#gt'),
        ]);
    }
}
