<?php

namespace App\Models\Cms;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Post extends Model
{
    protected $fillable= ['user_id', 'title', 'description'];

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    /**
     * Author post
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
