<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Sistema_Saldo\Balance;
use App\Models\Sistema_Saldo\Historic;
use App\Models\Cms\Post;
use App\Models\Cms\Comment;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'image',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /*
    /
    / Sempre criar dentro do controller de usuário os métodos que farão o relacionamento
    / entre o usuário e o objeto desejado
    /
    */

    public function balance()
    {
        return $this->hasOne(Balance::class);
    }

    public function historics()
    {
        return $this->hasMany(Historic::class);
    }

    public function getSender($sender)
    {
        return $this->where('name', 'LIKE', "%$sender%")
                ->orWhere('email', $sender)
                //->toSql(); posso usar para debugar a query no lugar dos dois abaixo.
                ->get()
                ->first();
    }


    public function posts()
    {
        return $this->hasMany(Post::class);
    }


    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
}
