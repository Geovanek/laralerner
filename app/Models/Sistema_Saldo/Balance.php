<?php

namespace App\Models\Sistema_Saldo;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use DB;

class Balance extends Model
{
    // private $table = 'balance';
    public $timestamps = false;

    public function deposit(float $value) : Array
    {
        // https://blog.especializati.com.br/database-transactions-no-laravel/
        DB::beginTransaction();

        $total_before = $this->amount ? $this->amount : 0;
        $this->amount += number_format($value, 2, '.', '');
        $deposit = $this->save();

        $historic = auth()->user()->historics()->create([
            /* sempre que eu passar uma array de itens, preciso ir na própria model em si, e criar
            / quais valores posso inserir e quais não, por questão de segurança
            */
            'type' => 'I',
            'amount' => $value,
            'total_before' => $total_before,
            'total_after' => $this->amount,
            'date' => date('Ymd'),
        ]);

        if ($deposit && $historic){

            DB::commit();

            return[
                'success' => true,
                'message' => 'Recarga realizada com sucesso.',
            ];
        } else {

            DB::rollback();

            return[
                'success' => false,
                'message' => 'Falha ao efetuar a recarga.',
            ];
        }
    }

    public function withdraw(float $value) : Array
    {
        if ($this->amount < $value)
            return [
                'success' => false,
                'message' => 'Saldo insuficiente',
            ];

        DB::beginTransaction();

        $total_before = $this->amount ? $this->amount : 0;
        $this->amount -= number_format($value, 2, '.', '');
        $withdraw = $this->save();

        $historic = auth()->user()->historics()->create([
            /* sempre que eu passar uma array de itens, preciso ir na própria model em si, e criar
            / quais valores posso inserir e quais não, por questão de segurança
            */
            'type' => 'O',
            'amount' => $value,
            'total_before' => $total_before,
            'total_after' => $this->amount,
            'date' => date('Ymd'),
        ]);

        if ($withdraw && $historic){

            DB::commit();

            return[
                'success' => true,
                'message' => 'Retirada realizada com sucesso.',
            ];
        } else {

            DB::rollback();

            return[
                'success' => false,
                'message' => 'Falha ao efetuar a retirada.',
            ];
        }
    }


    public function transfer(float $value, User $sender) : Array
    {
        if ($this->amount < $value)
            return [
                'success' => false,
                'message' => 'Saldo insuficiente',
            ];

        DB::beginTransaction();

        /*******************************************************************
        / Atualiza o próprio saldo
        ********************************************************************/
        $total_before = $this->amount ? $this->amount : 0;
        $this->amount -= number_format($value, 2, '.', '');
        $transfer = $this->save();

        $historic = auth()->user()->historics()->create([
            'type'                  => 'T',
            'amount'                => $value,
            'total_before'          => $total_before,
            'total_after'           => $this->amount,
            'date'                  => date('Ymd'),
            'user_id_transaction'   => $sender->id,
        ]);

        /*******************************************************************
        / Atualiza o saldo de quem está recebendo
        ********************************************************************/
        $sender_balance = $sender->balance()->firstOrCreate([]); //firstOrCreate entender bem isso
        $sender_total_before = $sender_balance->amount ? $sender_balance->amount : 0;
        $sender_balance->amount += number_format($value, 2, '.', '');
        $transfer_sender = $sender_balance->save();

        $historic_sender = $sender->historics()->create([
            'type' => 'I',
            'amount' => $value,
            'total_before' => $sender_total_before,
            'total_after' => $sender_balance->amount,
            'date' => date('Ymd'),
            'user_id_transaction' => auth()->user()->id,
        ]);

        if ($transfer && $historic && $transfer_sender && $historic_sender){

            DB::commit();

            return[
                'success' => true,
                'message' => 'Retirada realizada com sucesso.',
            ];
        }

        DB::rollback();

        return[
            'success' => false,
            'message' => 'Falha ao efetuar a retirada.',
        ];
    }
}
