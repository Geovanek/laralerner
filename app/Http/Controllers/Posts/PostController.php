<?php

namespace App\Http\Controllers\Posts;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Cms\Post; //chamo o model Post aqui para não precisar chamar nos métodos.

class PostController extends Controller
{
    private $post;
    /**
     * Criando método construtor para a model Post
     * não preciso ficar injetando ela em todos os outros métodos,
     * basta instanciar eles
     */
    public function __construct(Post $post)
    {
        $this->post = $post;
    }
    
    public function index() //(Post $post) injeto o model aqui para ficar mais fácil de trabalhar no método.
    {
        $posts = $this->post->with('comments')->paginate();

        return view('posts.index', compact('posts'));
    }


    public function show($id)
    {
        /**
         * Com uso do with (e o array) em um única consulta ao banco de dados
         * consigo retornar o author do post e todos os dados referntes aos
         * comentários, economizando muito recursos.
         */
        $post = $this->post->with(['comments.user', 'user'])->find($id); //where('id', $id)->fist();

        return view('posts.show', compact('post'));
    }
}
