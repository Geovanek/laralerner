<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//Preciso chamar a model balance para interagir com o controller
use App\Models\Sistema_Saldo\Balance;
use App\Models\Sistema_Saldo\historic;
//Chamar a classe de validação do request
use App\Http\Requests\MoneyValidationFormRequest;
use App\Models\User;

class BalanceController extends Controller
{
    private $totalPage = 5;

    public function index()
    {
        $balance = auth()->user()->balance;
        $amount = $balance ? $balance->amount : 0;

        return view('admin.balance.index', compact('amount'));
    }

    public function deposit()
    {
        return view('admin.balance.deposit');
    }

    public function depositStore(MoneyValidationFormRequest $request)
    {
        /*
        /
        / Laravel possue esse método firstOrCreate que automaticamente verifica
        / se já existe o cadastro no banco de dados, se não existir ele cria, se
        / existir ele implementa
        /
        */

        //Armazeno o saldo do usuário na variável $balance
        $balance = auth()->user()->balance()->firstOrCreate([]);
        //Chamo o método em si que faz o depósito
        //dd($balance->deposit($request->value));
        $response = $balance->deposit($request->value);

        if ($response['success'])
            return redirect()->route('admin.balance')
                             ->with('success', $response['message']);

            return redirect()
                        ->back()
                        ->with('error', $response['message']);
    }

    public function withdraw()
    {
        return view('admin.balance.withdraw');
    }

    public function withdrawStore(MoneyValidationFormRequest $request)
    {
        //dd($request->all());

        $balance = auth()->user()->balance()->firstOrCreate([]);
        //Chamo o método em si que faz o depósito
        //dd($balance->deposit($request->value));
        $response = $balance->withdraw($request->value);

        if ($response['success'])
            return redirect()->route('admin.balance')
                             ->with('success', $response['message']);

            return redirect()
                        ->back()
                        ->with('error', $response['message']);
    }

    public function transfer()
    {
        return view('admin.balance.transfer');
    }


    public function confirmTransfer(Request $request, User $user)
    {
        if (!$sender = $user->getSender($request->sender))
            return redirect()
                        ->back()
                        ->with('error', 'Usuário informado não encontrado!');

        if ($sender->id === auth()->user()->id)
            return redirect()
                        ->back()
                        ->with('error', 'Não pode transferir para você mesmo!');

        $balance = auth()->user()->balance;

        return view('admin.balance.confirm-transfer', compact('sender', 'balance'));
    }


    public function transferStore(MoneyValidationFormRequest $request, User $user)
    {
        if (!$sender = $user->find($request->sender_id))
            return redirect()->route('balance.transfer')
                            ->with('success', 'Recebedor não encontrado.');


        $balance = auth()->user()->balance()->firstOrCreate([]);
        //Chamo o método em si que faz o depósito
        //dd($balance->deposit($request->value));
        $response = $balance->transfer($request->value, $sender);

        if ($response['success'])
            return redirect()->route('admin.balance')
                            ->with('success', $response['message']);

            return redirect()->route('balance.transfer')
                            ->with('error', $response['message']);
    }

    public function historic(Historic $historic)
    {
        $historics = auth()
                    ->user()
                    ->historics()
                    ->with(['userSender'])
                    ->paginate($this->totalPage);

        $types = $historic->type();

        return view('admin.balance.historics', compact('historics', 'types'));
    }

    public function searchHistoric(Request $request, Historic $historic)
    {
        $dataForm = $request->except('_token');

        $historics = $historic->search($dataForm, $this->totalPage);

        $types = $historic->type();

        return view('admin.balance.historics', compact('historics', 'types', 'dataForm'));
    }
}
